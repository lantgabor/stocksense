from django.urls import reverse
from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase


class AuthenticatedTestCaseBase(APITestCase):
    def setUp(self):
        User = get_user_model()
        User.objects.create_user(email="trader@example.com", password="secret")

        url = reverse("login-obtain-token")
        data = {"email": "trader@example.com", "password": "secret"}
        response = self.client.post(url, data, format="json")

        access = response.data["access"]
        self.client.credentials(HTTP_AUTHORIZATION="Bearer " + access)


class UserManagerTests(TestCase):
    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(email="trader@example.com", password="secret")
        self.assertEquals(user.email, "trader@example.com")
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        self.assertIsNone(user.username)

        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email="")
        with self.assertRaises(ValueError):
            User.objects.create_user(email="", password="secret")

    def test_create_superuser(self):
        User = get_user_model()
        admin_user = User.objects.create_superuser(
            email="super@example.com", password="secret"
        )
        self.assertEqual(admin_user.email, "super@example.com")
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        self.assertIsNone(admin_user.username)

        with self.assertRaises(ValueError):
            User.objects.create_superuser(
                email="super@example.com", password="secret", is_superuser=False
            )


class AuthAPITests(APITestCase):
    def test_login(self):
        User = get_user_model()
        User.objects.create_user(email="trader@example.com", password="secret")

        url = reverse("login-obtain-token")
        data = {"email": "trader@example.com", "password": "secret"}
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        access = response.data["access"]

        # check the token
        self.client.credentials(HTTP_AUTHORIZATION="Bearer " + access)
        response = self.client.get(
            reverse("list-trader-stocks"),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_refresh(self):
        User = get_user_model()
        User.objects.create_user(email="trader@example.com", password="secret")

        data = {"email": "trader@example.com", "password": "secret"}
        response = self.client.post(reverse("login-obtain-token"), data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        refresh = response.data["refresh"]
        response = self.client.post(
            reverse("login-token-refresh"), {"refresh": refresh}, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
