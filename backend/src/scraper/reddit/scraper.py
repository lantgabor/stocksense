from datetime import datetime as dt
from django.utils import timezone
from django.utils.timezone import make_aware
from .models import RedditScraperState
from post.models import Post, PostComment
import json
import praw
import datetime
import pytz
import pathlib
from praw.models import MoreComments

utc = pytz.UTC
REDDIT_CONFIG_PATH = "config.json"


def get_date(submission):
    time = submission.created
    return datetime.datetime.fromtimestamp(time)


class RedditScraper:
    def __init__(self):
        self.reddit = None
        self.config = None
        self.config_path = REDDIT_CONFIG_PATH
        self.creds = {}
        self.setup()

    def setup(self):
        self.config = json.load(
            open(f"{pathlib.Path(__file__).parent.resolve()}/{self.config_path}")
        )
        self.creds = {
            "client_id": self.config["creditentials"]["client_id"],
            "secret_key": self.config["creditentials"]["secret_key"],
            "app_name": self.config["creditentials"]["app_name"],
            "username": self.config["creditentials"]["username"],
            "password": self.config["creditentials"]["password"],
        }
        try:
            self.reddit = praw.Reddit(
                client_id=self.creds["client_id"],
                client_secret=self.creds["secret_key"],
                user_agent=self.creds["app_name"],
                username=self.creds["username"],
                password=self.creds["password"],
            )
        except Exception as e:
            print(e)
            print("Reddit client error")

    def execute(self, from_date, to_date):
        now = dt.now(tz=timezone.utc)
        state = RedditScraperState.objects.first()

        if state is None or to_date > state.last_date:
            for stock in self.config["scraper_config"]["stocks"]:
                keywords = stock["keywords"]
                keywords.extend(self.config["scraper_config"]["common_keywords"])
                subreddits = stock["subreddits"]
                subreddits.extend(self.config["scraper_config"]["common_subreddits"])
                post_count = 0
                post_break = False
                for sub in subreddits:
                    subreddit = self.reddit.subreddit(sub)
                    if post_break:
                        break
                    for keyword in keywords:
                        if post_break:
                            break
                        for submission in subreddit.search(
                            keyword,
                            "timestamp:{0}..{1}".format(
                                to_date - from_date,
                                to_date,
                            ),
                        ):
                            # create the post
                            if get_date(submission):
                                post, created = Post.objects.update_or_create(
                                    provider_id="reddit",
                                    source_id=str(submission.id),
                                    defaults={
                                        "timestamp": make_aware(get_date(submission)),
                                        "stock": stock["name"],
                                        "title": submission.title,
                                        "body": submission.selftext,
                                    },
                                )
                                submission.comment_sort = "best"
                                i = 0
                                for top_level_comment in submission.comments:
                                    if isinstance(top_level_comment, MoreComments):
                                        continue
                                    if (
                                        not top_level_comment.stickied
                                        and str(top_level_comment.body) != "[deleted]"
                                    ):  # Filter deleted
                                        (
                                            comment,
                                            created,
                                        ) = PostComment.objects.update_or_create(
                                            post=post,
                                            source_id=str(top_level_comment.id),
                                            defaults={
                                                "timestamp": make_aware(
                                                    datetime.datetime.fromtimestamp(
                                                        top_level_comment.created_utc
                                                    )
                                                ),
                                                "body": str(top_level_comment.body),
                                            },
                                        )
                                        comment.save()

                                        i += 1
                                    if i == 10:
                                        break
                            post_count += 1
                            if post_count == 10:
                                post_break = True
                                break

            # update the state
            if state is not None:
                state.last_date = now
                state.save()
            else:
                state = RedditScraperState(last_date=to_date)
                state.save()
            RedditScraperState.objects.update_or_create(last_date=to_date)
