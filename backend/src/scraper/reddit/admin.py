from django.contrib import admin
from .models import RedditScraperState

admin.site.register(RedditScraperState)
