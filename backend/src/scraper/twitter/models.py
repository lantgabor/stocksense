from django.db import models


class TwitterScraperState(models.Model):

    last_date = models.DateTimeField(
        help_text="The timestamp of the lastly downloaded twitter post"
    )
