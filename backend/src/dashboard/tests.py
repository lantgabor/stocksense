from django.urls import reverse

# from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase
from stock.models import Stock, StockPrice, StockSentiment
from post.models import Post, PostComment

from model_bakery import baker
from user.tests import AuthenticatedTestCaseBase


class DashboardAPITests(APITestCase):
    def test_list_trader_stocks(self):

        # given
        for i in range(0, 10):
            stock = baker.make(Stock)
            baker.make(StockPrice, stock=stock, _quantity=100)

        # when
        response = self.client.get(reverse("list-trader-stocks"), format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 10)

    def test_list_trader_stocks_should_return_list_even_no_price_and_sentiment(self):

        # given
        for i in range(0, 5):
            baker.make(Stock)

        # when
        response = self.client.get(reverse("list-trader-stocks"), format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)

    def test_list_trader_stocks_should_contain_latest_price_and_sentiment(self):

        # given
        for i in range(0, 5):
            stock = baker.make(Stock)
            baker.make(StockPrice, stock=stock, _quantity=10)
            baker.make(StockSentiment, stock=stock, _quantity=10)

        # when
        response = self.client.get(reverse("list-trader-stocks"), format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)
        for item in response.data:

            source_stock = Stock.objects.filter(id=item["id"]).get()
            latest_price = (
                StockPrice.objects.filter(stock=source_stock)
                .order_by("-timestamp")
                .first()
            )
            latest_sentiment = (
                StockSentiment.objects.filter(stock=source_stock)
                .order_by("-timestamp")
                .first()
            )

            self.assertEqual(item["price"], latest_price.price)
            self.assertEqual(item["sentiment"], latest_sentiment.sentiment)

    def test_list_trader_stocks_with_title_filter(self):

        # given
        stocks = baker.make(Stock, _quantity=10)

        # when
        url = reverse("list-trader-stocks")
        url += f"?filter={stocks[0].title}"
        response = self.client.get(url, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_list_trader_stocks_with_symbol_filter(self):

        # given
        stocks = baker.make(Stock, _quantity=10)

        # when
        url = reverse("list-trader-stocks")
        url += f"?filter={stocks[0].symbol}"
        response = self.client.get(url, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_trader_stock_details(self):

        # given
        stocks = baker.make(Stock, _quantity=10)
        stock = stocks[2]

        # when
        url = reverse("trader-stock-details", kwargs={"symbol": stock.symbol})
        response = self.client.get(url, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_stock = response.data

        self.assertEqual(response_stock["name"], stock.title)
        self.assertEqual(response_stock["symbol"], stock.symbol)

    def test_query_stock_graph_when_no_parameters(self):

        # given
        stock = baker.make(Stock)
        baker.make(StockPrice, stock=stock, _quantity=50)
        baker.make(StockSentiment, stock=stock, _quantity=60)

        url = reverse("trader-stock-graph", kwargs={"id": stock.id})

        # when
        response = self.client.get(url, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        prices = response.data["prices"]
        self.assertEqual(len(prices), 50)
        sentiments = response.data["sentiments"]
        self.assertEqual(len(sentiments), 60)

    def test_query_stock_graph_when_parameters(self):

        # given
        stock = baker.make(Stock)
        baker.make(StockPrice, stock=stock, _quantity=50)
        baker.make(StockSentiment, stock=stock, _quantity=60)

        url = reverse("trader-stock-graph", kwargs={"id": stock.id})
        url += "?from=2021-11-11T11:45&to=2021-11-11T11:35"

        # when
        response = self.client.get(url, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        prices = response.data["prices"]
        self.assertEqual(len(prices), 0)
        sentiments = response.data["sentiments"]
        self.assertEqual(len(sentiments), 0)


class DashboardTraderTests(AuthenticatedTestCaseBase):
    def test_list_trader_stocks_should_contain_starred(self):

        # given
        stocks = baker.make(Stock, _quantity=10)

        starred_stock_id = stocks[2].id
        self.star_unstar_stock(starred_stock_id, starred=True)

        # when
        response = self.client.get(reverse("list-trader-stocks"), format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 10)
        for response_stock in response.data:
            should_starred = response_stock["id"] == starred_stock_id
            self.assertEqual(response_stock["is_starred"], should_starred)

    def test_list_trader_stocks_should_contain_only_starred_when_starred_type_set(self):

        # given
        stocks = baker.make(Stock, _quantity=10)

        starred_stock_id = stocks[2].id
        self.star_unstar_stock(starred_stock_id, starred=True)

        # when
        url = reverse("list-trader-stocks")
        url += "?type=starred"
        response = self.client.get(url, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["is_starred"], True)

    def test_trader_details_should_contain_starred(self):
        # given
        stocks = baker.make(Stock, _quantity=10)

        starred_stock = stocks[2]
        self.star_unstar_stock(starred_stock.id, starred=True)

        # when
        response = self.client.get(
            reverse("trader-stock-details", kwargs={"symbol": starred_stock.symbol}),
            format="json",
        )

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_stock = response.data
        self.assertEqual(response_stock["id"], starred_stock.id)
        self.assertEqual(response_stock["is_starred"], True)

    def test_list_trader_stocks_with_starred_order(self):

        # given
        stocks = baker.make(Stock, _quantity=10)
        for i in range(0, 10):
            if i % 2 == 0:
                self.star_unstar_stock(stocks[i].id, starred=True)

        # when
        url = reverse("list-trader-stocks")
        url += "?order=starred"
        response = self.client.get(url, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        items = response.data
        self.assertEqual(len(items), 10)

        # start with the starred items
        for i in range(0, 5):
            self.assertTrue(items[i]["is_starred"])
        self.assertFalse(items[5]["is_starred"])

    def test_list_trader_stocks_with_eligibility_order(self):

        # given
        for i in range(0, 10):
            baker.make(Stock, eligibility=i * 0.1)

        # when
        url = reverse("list-trader-stocks")
        url += "?order=promising"
        response = self.client.get(url, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        items = response.data
        self.assertEqual(len(items), 10)

        # start with the starred items
        for i in range(1, 10):
            self.assertTrue(items[i - 1]["eligibility"] >= items[i]["eligibility"])

    def star_unstar_stock(self, id, starred):
        url = reverse("trader-star-unstar-stock", kwargs={"id": id})
        self.client.put(url, {"starred": starred}, format="json")


class DashboardStockPostsAPITests(APITestCase):
    def test_stock_associated_posts(self):

        # given
        stock = baker.make(Stock)
        posts = baker.make(Post, stock=stock.symbol, _quantity=50)
        for post in posts:
            baker.make(PostComment, post=post, _quantity=10)

        url = reverse("trader-stock-posts", kwargs={"symbol": stock.symbol})

        # when
        response = self.client.get(url, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        post_items = response.data

        self.assertEqual(len(post_items), 5)
        for post_item in post_items:
            self.assertEqual(len(post_item["comments"]), 5)
