from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from stock.models import Stock
from .serializers import RegisterTraderSerializer


class RegisterTraderView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = RegisterTraderSerializer


class StarUnstarTraderStockView(APIView):

    permission_classes = (IsAuthenticated,)

    def put(self, request, id=0, format="json"):

        trader = request.user.trader
        stock = Stock.objects.get(id=id)

        params_serializer = self.StarUnstarSerializer(data=request.data)
        if not params_serializer.is_valid():
            raise ValueError("Invalid input parameters", params_serializer.errors)

        to_star = params_serializer.validated_data["starred"]
        if to_star:
            trader.starred.add(stock)
        else:
            trader.starred.remove(stock)

        return Response(True)

    def get(self, request, id=0):

        trader = request.user.trader
        is_starred = trader.starred.filter(id=id).exists()

        return Response(is_starred)

    class StarUnstarSerializer(serializers.Serializer):
        starred = serializers.BooleanField()
