from django.db import models
from django.utils.translation import gettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from user.models import CustomUser
from stock.models import Stock


class Trader(models.Model):
    class Plan(models.TextChoices):

        NONE = "none", _("None")
        FREE = "free", _("Free")
        STANDARD = "standard", _("Standard")

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    plan = models.CharField(max_length=255, choices=Plan.choices, default=Plan.FREE)
    starred = models.ManyToManyField(
        Stock, blank=True, related_name="traders_starred", help_text="Starred stocks"
    )


# automatically create associated Trader with free plan after creating the user
@receiver(post_save, sender=CustomUser)
def create_trader(sender, instance, created, **kwargs):
    if created:
        Trader.objects.create(user=instance, plan="free")
