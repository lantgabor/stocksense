from django.db import models


class PostProvider(models.Model):

    id = models.CharField(
        max_length=100,
        primary_key=True,
        help_text="The unique identifier of the provier",
    )
    name = models.CharField(
        max_length=200, help_text="The name of the provider, e.g. Reddit"
    )


class Post(models.Model):
    stock = models.CharField(
        max_length=255, help_text="The stock of the post", default=""
    )
    provider = models.ForeignKey(
        PostProvider, help_text="The post provider", on_delete=models.PROTECT
    )
    source_id = models.CharField(
        max_length=255, help_text="The external identifier of the post, e.g. tweet id"
    )
    timestamp = models.DateTimeField(help_text="The timestamp of the post")
    title = models.CharField(max_length=1024, help_text="The title of the post")
    body = models.TextField(help_text="The body of the post")

    ordering = ["timestamp"]

    class Meta:
        unique_together = (
            "source_id",
            "provider",
        )


class PostComment(models.Model):
    timestamp = models.DateTimeField(help_text="The timestamp of the comment")
    title = models.CharField(
        max_length=1024, blank=True, help_text="The title of the comment (optional)"
    )
    body = models.TextField(help_text="The body of the comment")
    post = models.ForeignKey(
        Post,
        help_text="The associated post",
        on_delete=models.PROTECT,
        related_name="comments",
    )
    source_id = models.CharField(
        max_length=255,
        help_text="The external identifier of the comment, e.g. tweet id",
    )
    ordering = ["timestamp"]

    class Meta:
        unique_together = (
            "source_id",
            "post",
        )
