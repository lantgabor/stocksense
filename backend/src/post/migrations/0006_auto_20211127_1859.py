# Generated by Django 3.2.8 on 2021-11-27 18:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("post", "0005_alter_postcomment_post"),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name="post",
            unique_together={("source_id", "provider")},
        ),
        migrations.AlterUniqueTogether(
            name="postcomment",
            unique_together={("source_id", "post")},
        ),
    ]
