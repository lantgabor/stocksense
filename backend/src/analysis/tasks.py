from .analysis import SentimentAnalysis
import datetime


def start_sentiment_analysis():
    to_date = datetime.datetime.now()
    from_date = to_date - datetime.timedelta(days=2)  # for last 2 days
    analysis = SentimentAnalysis()
    print("start_sentiment_analysis", from_date, to_date)
    analysis.execute(from_date, to_date)
