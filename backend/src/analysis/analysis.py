import pandas as pd
from post.models import Post
from stock.models import StockSentiment, Stock
from scraper.twitter.scraper import stocks_abbr
import nltk
import ssl
from django.utils.timezone import make_aware


class SentimentAnalysis:
    def __init__(self):

        try:
            _create_unverified_https_context = ssl._create_unverified_context
        except AttributeError:
            pass
        else:
            ssl._create_default_https_context = _create_unverified_https_context
        nltk.download("vader_lexicon")
        from nltk.sentiment.vader import SentimentIntensityAnalyzer

        self.sid = SentimentIntensityAnalyzer()

    def sentiment_analysis(self, list_of_text, mean=True):
        """
        Function to calculate the sentiment of a text using Vader
        :param list_of_text: A list or series of Texts (strings)
        :param mean: Boolean whether to return the mean of all tweets or single compound scores
        :return: One or multiple compound scores ranging between -1 and 1,
        where -1 is negative and +1 is positive sentiment
        """
        scores = list_of_text.apply(lambda text: self.sid.polarity_scores(text))
        compound = scores.apply(lambda score_dict: score_dict["compound"])
        if mean:
            return compound.mean()
        else:
            return compound

    def execute(self, from_date, to_date):
        from_date = make_aware(from_date)
        to_date = make_aware(to_date)
        for stock_title, stock_abbr in stocks_abbr.items():
            input_data = []
            posts = Post.objects.filter(
                stock=stock_abbr, timestamp__range=(from_date, to_date)
            )
            input_data += [post.title for post in posts if post.title]
            input_data += [post.body for post in posts if post.body]
            for post in posts:
                comments = post.comments.all()
                input_data += [
                    comment.title
                    for comment in comments
                    if comment.title
                    and comment.timestamp < to_date
                    and comment.timestamp > from_date
                ]
                input_data += [comment.body for comment in comments if comment.body]
            if input_data:
                input_data = pd.Series(input_data)
                stock = Stock.objects.get_or_create(symbol=stock_abbr)
                stock = stock[0]
                if stock:
                    if not stock.title:
                        stock.title = stock_title
                        stock.save()
                    StockSentiment.objects.create(
                        stock=stock,
                        timestamp=to_date,
                        sentiment=self.sentiment_analysis(input_data),
                    )
