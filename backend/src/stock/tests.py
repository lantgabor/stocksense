from django.utils.timezone import make_aware
from datetime import datetime, timedelta
from django.test import TestCase
from model_bakery import baker

from .models import Stock, StockPrice, StockSentiment
from .eligibility_calculator import StockChangeCalculator


class StockChangeCalculatorTest(TestCase):
    def test_calculate_percent_change(self):

        # given
        cases = [(10, 15, 0.5), (20, 10, -0.5)]

        # when
        calculator = StockChangeCalculator()
        for case in cases:
            change = calculator.calculate_percent_change(case[0], case[1])
            # then
            self.assertEquals(change, case[2])

    def test_calculate_stock_min_max(self):

        # given
        price_sentiment_decrease_case = (
            [14, 20, 12, 11, 10],
            [0.1, 0.5, 0.0, -0.1, -1],
            {
                "price": {"min": 10, "max": 20, "change": -0.5},
                "sentiment": {"min": -1, "max": 0.5, "change": -0.75},
            },
        )

        price_sentiment_increase_case = (
            [11, 10, 12, 11, 15],
            [0.1, 0.2, 0.0, -0.1, 0.4],
            {
                "price": {"min": 10, "max": 15, "change": 0.5},
                "sentiment": {"min": -0.1, "max": 0.4, "change": 0.25},
            },
        )

        missing_price_case = (
            [None, None, None, None, None],
            [0.1, 0.2, 0.0, -0.1, -1],
            {
                "price": {"min": None, "max": None, "change": 0},
                "sentiment": {"min": -1, "max": 0.2, "change": -0.6},
            },
        )

        missing_sentiment_case = (
            [14, 20, 12, 11, 10],
            [None, None, None, None, None],
            {
                "price": {"min": 10, "max": 20, "change": -0.5},
                "sentiment": {"min": None, "max": None, "change": 0},
            },
        )

        cases = [
            price_sentiment_decrease_case,
            price_sentiment_increase_case,
            missing_price_case,
            missing_sentiment_case,
        ]

        expected = []

        for case in cases:
            # given
            stock = baker.make(Stock)

            date = datetime(2001, 1, 1)
            prices = case[0]
            sentiments = case[1]

            for i in range(0, 5):
                date = date + timedelta(minutes=15)
                if prices[i] is not None:
                    baker.make(
                        StockPrice,
                        stock=stock,
                        timestamp=make_aware(date),
                        price=prices[i],
                    )
                if sentiments[i] is not None:
                    baker.make(
                        StockSentiment,
                        stock=stock,
                        timestamp=make_aware(date),
                        sentiment=sentiments[i],
                    )

            expected.append({"id": stock.id, "values": case[2]})

        calculator = StockChangeCalculator()

        # when
        since = datetime(2001, 1, 1, 0, 30, 0)
        result = calculator.calculate(since)

        # then
        for item in result:
            expected_item = next((x for x in expected if x["id"] == item["id"]))[
                "values"
            ]

            self.assertEquals(item["price"], expected_item["price"])
            self.assertEquals(item["sentiment"], expected_item["sentiment"])
