from datetime import datetime
from django.conf import settings
from django.utils.timezone import make_aware
from django.utils import timezone
from alpha_vantage.timeseries import TimeSeries
from alpha_vantage.fundamentaldata import FundamentalData

from .models import Stock, StockPrice


class StockScraper:
    def __init__(self):
        config = settings.STOCK_SCRAPER
        self.key = config["alpha_vantage_key"]
        self.symbols = config["symbols"]
        self.interval = config["interval"]
        self.refresh_stock_seconds = config["refresh_stock_seconds"]

        self.time_series = TimeSeries(key=self.key, output_format="pandas")
        self.fundamental_data = FundamentalData(key=self.key)

    def execute(self):

        for symbol in self.symbols:
            now = datetime.now(tz=timezone.utc)

            stock = self.collect_symbol(symbol)
            if stock is None:
                continue

            if (
                stock.last_scrape_time is not None
                and (now - stock.last_scrape_time).total_seconds()
                <= self.refresh_stock_seconds
            ):
                continue

            df = self.collect_intraday(symbol)
            if df is None:
                print(f"{symbol} temporary download error, will try later")
                continue

            self.save_prices(stock, df, now)

    def collect_symbol(self, symbol):

        obj = Stock.objects.filter(symbol=symbol).first()
        if obj is None:
            try:
                details, _ = self.fundamental_data.get_company_overview(symbol)
            except ValueError:
                print(f"{symbol} temporary download error, will try later")
                return None

            name = details["Name"]
            description = (
                details["Description"] if details["Description"] is not None else ""
            )

            Stock.objects.create(symbol=symbol, title=name, description=description)

        return obj

    def collect_intraday(self, symbol):

        try:
            df, meta = self.time_series.get_intraday(
                symbol, outputsize="compact", interval=self.interval
            )

            return df
        except ValueError:
            return None

    def save_prices(self, stock, df, now):

        current_latest = stock.stockprice_set.order_by("timestamp").last()
        current_latest_timestamp = (
            current_latest.timestamp if current_latest is not None else None
        )

        for index, row in df.iterrows():
            timestamp = make_aware(index.to_pydatetime())
            close = row["4. close"]

            if (
                current_latest_timestamp is not None
                and timestamp <= current_latest_timestamp
            ):
                continue

            print("creating", timestamp)
            price = StockPrice(timestamp=timestamp, price=close, stock=stock)
            price.save()

        stock.last_scrape_time = now
        stock.save()
