from django.db import models


class Stock(models.Model):
    symbol = models.CharField(
        max_length=10, help_text="The stock's symbol, e.g. AAPL", unique=True
    )
    title = models.CharField(
        max_length=200, help_text="The stock's name, e.g. Apple Inc."
    )
    description = models.CharField(
        max_length=1024, help_text="The stock's description", default=""
    )
    last_scrape_time = models.DateTimeField(help_text="Last scrape time", null=True)

    eligibility = models.FloatField(
        help_text="The eligibility score based on recent price and sentiment change",
        default=0.0,
    )

    class Meta:
        ordering = ["symbol"]
        indexes = [models.Index(fields=["title"]), models.Index(fields=["description"])]

    def __str__(self):
        return f"{self.title} ({self.symbol})"


class StockPrice(models.Model):
    """Represents the price of a stock for a given timestamp"""

    stock = models.ForeignKey(Stock, on_delete=models.PROTECT)
    timestamp = models.DateTimeField(help_text="The timestamp for the price")
    price = models.FloatField(help_text="The price")

    class Meta:
        indexes = [models.Index(fields=["timestamp"])]

    def __str__(self):
        return f"{self.timestamp}: {self.price}"


class StockSentiment(models.Model):
    """Represents the sentiment of a stock for a given timestamp"""

    stock = models.ForeignKey(Stock, on_delete=models.PROTECT)
    timestamp = models.DateTimeField(help_text="The timestamp for the sentiment")
    sentiment = models.FloatField(help_text="The sentiment")

    class Meta:
        indexes = [models.Index(fields=["timestamp"])]
