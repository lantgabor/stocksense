-r requirements.txt
model_bakery==1.3.3
flake8==4.0.1
ipython>=7.29.0
black==21.11b1
