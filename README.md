# StockSense

The product does sentiment analysis on social media data (Twitter, Reddit) to track how people talk about a certain stock to help day traders. With the rise of meme stocks such as GME and AMC, this could be helpful in getting an overview if people are positive or negative about it.

## Development

```bash
docker-compose up
```

If something does not work, it might be because the Dockerfile of the dev environment changed. In this case you need to rebuild the images using:

```bash
docker-compose up --build
```

If there is a problem starting the containers, please try to delete and recreate them:

```bash
docker-compose rm
docker-compose up --build
```

Then you can access:

* frontend: http://localhost:3000
* backend: http://localhost:8000
  * admin:
    * http://localhost:8000/admin
    * username: `admin@stocksense.ai`
    * password: `secret`
* adminer (db administration): http://localhost:8080
* mailcatcher (for local email sending): http://localhost:1080

### Backend development

You can enter to the backend container using:

```bash
docker exec -it stocksense_backend_1 bash
```

#### Run a single script

For running single script, e.g. scraper script you can use django shell with ipython that supports autoreload.

After entering the container (see above):

```bash
python manage.py shell_plus --ipython
```

In IPython shell:

```python
# to enable autoreload
%load_ext autoreload
%autoreload 2

# start a script
from scraper.tasks import scrape_reddit
scrape_reddit()

# change something then

scrape_reddit()
```

#### Start scheduler

You can start the task scheduler using:

```
python manage.py qcluster
```

#### Check code style

To start PEP8 checker, use:

```bash
flake8
```

This check will also run automatically on build pipeline in **test** phase.

#### DB administration

You can access the local PostgreSQL by adminer: http://localhost:8080

At connection parameters set:

* System: PostgreSQL
* Server: db
* Username: stocksense
* Password: secret

## Production

The up-to-date version of the `main` branch is available here:

* Site: https://stocksense.westeurope.azurecontainer.io
* Django backend admin: https://stocksense.westeurope.azurecontainer.io/admin
* PostgreSQL administration: https://stocksense.westeurope.azurecontainer.io/adminer

## Contributing

GitLab flow is used with small feature branches which are merged to the `main`.

# Backend API docs

## URLs

Usage of the APIs can be viewed if you open in browser, e.g. http://localhost:8000/bapi/trader/register

Or you can use [httpie](https://httpie.io/) to test; you can install it using `pip install httpie`

### Authentication

#### Registration

* URL: `/bapi/trader/register`
* Example
  ```
  http post http://localhost:8000/bapi/trader/register email=user@example.com first_name=First last_name=Last password=NonSecret123 password2=NonSecret123 trader:='{"plan": "free"}'
  ```

#### JWT login

* URL: `/bapi/auth/login`
* Example:
  ```
  http post http://localhost:8000/bapi/auth/login email=user@example.com password=NonSecret123
  ```

#### JWT refresh

* URL: `/bapi/auth/login/refresh`
* Example:
  ```
  http post http://localhost:8000/bapi/auth/login/refresh refresh=<REFRESH_TOKEN>
  ```

### Trader

#### Star/unstar

**NOTE**: authentication required

* URL: `/bapi/trader/stocks/<id>/starred`
* Example:
  ```
  http put http://localhost:8000/bapi/trader/stocks/1/starred starred=true 'Authorization:Bearer <ACCESS_TOKEN>'
  ```

### Dashboard

**NOTE**: authentication optional now

#### Stock list

* URL: `/bapi/dashboard/stocks`
* optional parameters:
  * `filter`: Filters by stock symbol and name
  * `type`: Filter the list to favorites or promising, possible values: `all`, `starred`
  * `order`: Order the list by name, starred state or by promising level: `name`, `starred`, `promising`
* Example:
  ```
  http http://localhost:8000/bapi/dashboard/stocks filter==AA type==all 'Authorization:Bearer <ACCESS_TOKEN>'
  ```

#### Stock details

* URL: `/bapi/dashboard/stocks/<SYMBOL>`
* Example:
  ```
  http http://localhost:8000/bapi/dashboard/stocks/AAPL 'Authorization:Bearer <ACCESS_TOKEN>'
  ```

#### Stock post examples

* URL: `/bapi/dashboard/stocks/<SYMBOL>/posts`
* Example:
  ```
  http http://localhost:8000/bapi/dashboard/stocks/AAPL/posts
  ```

#### Stock graph

* URL: `/bapi/dashboard/stock/<id>/graph`
* optional parameters:
  * `from`: Start timestamp
  * `to`: End timestamp
* Example:
  ```
  http http://localhost:8000/bapi/dashboard/stocks/1/graph from==2021-11-11T11:45 to==2021-12-11T12:15 'Authorization:Bearer <ACCESS_TOKEN>'
  ```