#!/usr/bin/env sh

# set -o pipefail  # trace ERR through pipes
# set -o errtrace  # trace ERR through 'time command' and other functions
# set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
# set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

set -u
set -e

# docker cli
wget https://github.com/docker/compose-cli/releases/download/v1.0.17/docker-linux-amd64
mv docker-linux-amd64 docker-cloud
chmod +x docker-cloud
ln -s /usr/local/bin/docker /usr/local/bin/com.docker.cli

# docker compose as plugin for the cli
wget https://github.com/docker/compose/releases/download/1.29.2/docker-compose-linux-x86_64
mv docker-compose-linux-x86_64 docker-compose
mkdir -p ~/.docker/cli-plugins
mv docker-compose ~/.docker/cli-plugins/
chmod +x ~/.docker/cli-plugins/docker-compose
ls ~/.docker/cli-plugins

# azure login
./docker-cloud login azure --client-id $AZURE_CLIENT_ID --tenant-id $AZURE_TENANT_ID --client-secret $AZURE_CLIENT_SECRET

# context creation and switch
./docker-cloud context create aci stocksense --resource-group stocksense
./docker-cloud context use stocksense

# create volume
./docker-cloud --context stocksense volume create traefik-volume --storage-account stocksense || echo 'already exists'

# save current acme.json
./docker-cloud exec stocksense_traefik "copy-out.sh" || echo 'traefik container does not exist yet, skipping'

# release
./docker-cloud compose -f production-stack-compose.yml up

# info
./docker-cloud ps
