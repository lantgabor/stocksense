#!/bin/sh

echo Running child entrypoint initialization steps here
copy-in.sh

exec /entrypoint.sh "$@"
