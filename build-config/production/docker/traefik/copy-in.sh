#!/bin/sh
mkdir -p /etc/traefik/acme
if [ -e /acme-store/acme.json ]
then
  cp -f /acme-store/acme.json /etc/traefik/acme/acme.json
  chmod 600 /etc/traefik/acme/acme.json
fi
