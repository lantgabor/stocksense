import tweepy
import pandas as pd

consumer_key = "XUo4KXp1UqoOvLZ0b19oUHuUR"
consumer_secret = "XfwmaRhwH729toSiHuAdtHSiUwWzlbmLoz61eXcKpNg0OiD562"
access_token = "1455537243989450762-z11DYKcUagQYYqDej1xJGHUSU4MAlS"
access_token_secret = "13Gf7sE8OWaZz8fcyl7GG28dPRJF80eEFR6RJjvEFTUXV"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)  # tweepy.api.API

try:
    api.verify_credentials()
    print("Authentication OK")
except:
    print("Error during authentication")

# List of stocks to collect tweets for
stocks = [
    "Apple",
    "NVIDIA",
    "Tesla",
    "AMD",
    "Amazon",
    "Coinbase",
    "Microsoft",
    "Facebook",
    "Upstart",
    "Roblox",
    "Google",
    "Lucid Motors",
    "Gamestop",
    "AMC",
    "SmileDirectClub",
    "Rocket Lab",
    "Blackberry",
    "Beyond Meat",
]


def tweet_collector(stocks=stocks, num_of_tweets=15):
    """
    stocks: Python list of stocks that we want to grab tweets for
    num_of_tweets: Number of tweets we want to collect (starting with most recent ones)
    Returns: a list with dimensions (num_of_tweets*len(stocks), 3) that saves
    the stock, timestamp and tweet IF the tweet is in English language.
    A stream gives a random selection (1% of tweets). We decided to collect a mix of the most popular tweets
    and recent ones, as this is more representative of general sentiment than a random selection
    in our opinion.
    """
    tweets_list = []
    stocks = [stock + " -filter:retweets" for stock in stocks]  # Filter out retweets
    for stock in stocks:
        tweets = api.search_tweets(
            q=str(stock), count=num_of_tweets, lang="en", result_type="mixed"
        )
        for tweet in tweets:
            tweets_list.append([stock[:-16], tweet.created_at, tweet.text])
    return tweets_list


tweets_data = tweet_collector(stocks)

# Convert to pandas column
# Uncomment if necessary
# df = pd.DataFrame(tweets_data, columns=['Stock', 'Timestamp', 'Text'])
