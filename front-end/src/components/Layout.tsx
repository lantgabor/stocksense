import React, { ReactNode } from "react";

import { Navbar } from "@/components/navbar/Navbar";

type Props = {
	children?: ReactNode;
};

export const Layout = ({ children }: Props) => (
	<div className="min-h-screen text-gray-700 bg-gradient-to-b from-secondary-500 to-primary-900">
		<header>
			<Navbar />
		</header>
		{children}
	</div>
);
