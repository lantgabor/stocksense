import Image from "next/image";
import React from "react";
import { FaArrowRight } from "react-icons/fa";

export const Mobile = () => {
	return (
		<div>
			<section className="py-16 mx-auto max-w-6xl text-other-500">
				<div className="grid grid-cols-1 md:grid-cols-2 items-center px-4 sm:px-6 lg:px-8 mx-auto mt-8 md:mt-2">
					<div className="hidden md:block pb-6 mx-auto">
						<Image
							className="shadow-lg "
							src="/static/images/mobile.png"
							alt="img-mobile"
							width={337}
							height={704}
						/>
					</div>
					<div className="text-left">
						<h2 className="pb-14 text-4xl font-extrabold tracking-tight text-other-200">
							Manage your interests with better predicitons
						</h2>
						<ul className="space-y-6">
							<li className="flex space-x-4">
								<div className="text-primary-500">&bull;</div>
								<span>Follow stocks or cryptocurrecies of your choosing</span>
							</li>
							<li className="flex space-x-4">
								<div className="text-primary-500">&bull;</div>
								<span>Focused view to track your progress</span>
							</li>
							<li className="flex space-x-4">
								<div className="text-primary-500">&bull;</div>
								<span>
									Get a condensed view of the current state of the market
								</span>
							</li>
							<li className="flex items-center space-x-2 text-primary-500">
								<a role="button" className="">
									Read more
								</a>
								<FaArrowRight className="text-xs" />
							</li>
						</ul>
					</div>
				</div>
			</section>
		</div>
	);
};
