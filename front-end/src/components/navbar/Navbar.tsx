import { Disclosure } from "@headlessui/react";
import { MenuIcon, XIcon } from "@heroicons/react/outline";
import Link from "next/link";
import { useRouter } from "next/router";
import { useSession } from "next-auth/client";

import { classNames } from "@/helpers/classNames";

import { PrimaryButton } from "../buttons/PrimaryButton";
import { ProfileDropdown } from "./ProfileDropdown";

type NavItem = {
	name: string;
	href: string;
	visible: "always" | "authenticated" | "not-authenticated";
};

const navigation: NavItem[] = [
	{ name: "Home", href: "/", visible: "always" },
	{ name: "Stocks", href: "/stocks", visible: "authenticated" },
	{ name: "About", href: "/about", visible: "always" },
	{ name: "Pricing", href: "/pricing", visible: "always" },

	{ name: "Join Now", href: "/register", visible: "not-authenticated" },
];

export const Navbar = () => {
	const router = useRouter();
	const [session, loading] = useSession();
	const isLoggedIn = !!session?.user;

	const filteredNavigation = navigation.filter((x) => {
		if (loading) {
			return x.visible === "always";
		}
		return (
			x.visible === "always" ||
			(x.visible === "authenticated" && isLoggedIn) ||
			(x.visible === "not-authenticated" && !isLoggedIn)
		);
	});

	return (
		<Disclosure as="nav">
			{({ open }) => (
				<>
					<div className="px-2 sm:px-6 lg:px-8 mx-auto max-w-7xl">
						<div className="flex relative justify-between items-center h-16">
							<div className="flex sm:hidden absolute inset-y-0 left-0 items-center">
								{/* Mobile menu button */}
								<Disclosure.Button className="inline-flex justify-center items-center p-2 text-primary-400 hover:text-other-200 hover:bg-secondary-700 rounded-md focus:ring-2 focus:ring-inset focus:ring-white focus:outline-none">
									<span className="sr-only">Open main menu</span>
									{open ? (
										<XIcon className="block w-6 h-6" aria-hidden="true" />
									) : (
										<MenuIcon className="block w-6 h-6" aria-hidden="true" />
									)}
								</Disclosure.Button>
							</div>
							<div className="flex flex-1 justify-center sm:justify-start items-center sm:items-stretch ">
								<div className="flex flex-shrink-0 items-center">
									<svg
										id="Layer_1"
										data-name="Layer 1"
										xmlns="http://www.w3.org/2000/svg"
										viewBox="0 0 14.98 22"
										className="w-7 h-7 text-other-200 fill-current"
									>
										<polygon points="7.23 12.86 5.63 11.26 6.32 10.57 7.06 9.83 4.24 7.02 2.81 8.45 0 11.26 2.81 14.07 4.42 15.68 7.23 18.49 8.45 19.7 11.26 16.89 10.05 15.68 7.23 12.86" />
										<polygon points="12.17 7.92 10.57 6.32 7.75 3.51 6.54 2.29 3.72 5.11 4.94 6.32 7.75 9.14 9.35 10.74 8.66 11.43 7.92 12.17 10.74 14.98 12.17 13.55 14.98 10.74 12.17 7.92" />
										<rect
											x="6.62"
											y="18.19"
											width="3.98"
											height="3.98"
											transform="translate(-16.64 11) rotate(-45)"
										/>
										<rect
											x="14.15"
											y="1.82"
											width="3.98"
											height="3.98"
											transform="translate(-2.85 11.53) rotate(-45)"
										/>
									</svg>

									<div className="hidden lg:block w-auto h-8" />
								</div>
								<div className="hidden sm:block pl-5">
									<div className="flex space-x-4">
										{filteredNavigation.map((item) => (
											<Link href={item.href} key={item.name}>
												<a
													key={item.name}
													className={classNames(
														item.href === router.asPath
															? "bg-secondary-900 text-other-200"
															: "text-primary-300 hover:bg-secondary-700 hover:text-other-200",
														"px-3 py-2 rounded-md text-sm font-medium",
													)}
													aria-current={
														item.href === router.asPath ? "page" : undefined
													}
												>
													{item.name}
												</a>
											</Link>
										))}
									</div>
								</div>
							</div>
							<div className="flex absolute sm:static sm:inset-auto inset-y-0 right-0 items-center pr-2 sm:pr-0 sm:pl-6 space-x-3">
								{/* <Notification /> */}
								{/* Profile dropdown */}
								{isLoggedIn ? (
									<ProfileDropdown />
								) : (
									<div className="dark">
										<Link href="/login" passHref>
											<PrimaryButton intent="danger">Sign in</PrimaryButton>
										</Link>
									</div>
								)}
							</div>
						</div>
					</div>

					<Disclosure.Panel className="sm:hidden">
						<div className="px-2 pt-2 pb-3 space-y-1">
							{navigation.map((item) => (
								<Link href={item.href} key={item.name}>
									<a
										key={item.name}
										className={classNames(
											item.href === router.asPath
												? "bg-secondary-900 text-other-200"
												: "text-primary-300 hover:bg-secondary-700 hover:text-other-200",
											"block px-3 py-2 rounded-md text-base font-medium",
										)}
										aria-current={
											item.href === router.asPath ? "page" : undefined
										}
									>
										{item.name}
									</a>
								</Link>
							))}
						</div>
					</Disclosure.Panel>
				</>
			)}
		</Disclosure>
	);
};

/* function Notification() {
 * 	return (
 * 		<button
 * 			type="button"
 * 			className="p-1 text-primary-400 hover:text-other-200 bg-secondary-800 rounded-full focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800 focus:outline-none"
 * 		>
 * 			<span className="sr-only">View notifications</span>
 * 			<BellIcon className="w-6 h-6" aria-hidden="true" />
 * 		</button>
 * 	);
 * }
 *  */
