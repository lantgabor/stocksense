import { Menu, Transition } from "@headlessui/react";
import Image from "next/image";
import { useRouter } from "next/router";
import { signOut } from "next-auth/client";
import { Fragment } from "react";

import { classNames } from "@/helpers/classNames";

export const ProfileDropdown = () => {
	return (
		<Menu as="div" className="relative pl-3">
			<div>
				<Menu.Button className="flex text-sm bg-secondary-800 rounded-full focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800 focus:outline-none">
					<span className="sr-only">Open user menu</span>
					<Image
						className="w-8 h-8 rounded-full"
						src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
						alt=""
						width={32}
						height={32}
					/>
				</Menu.Button>
			</div>
			<Transition
				as={Fragment}
				enter="transition ease-out duration-100"
				enterFrom="transform opacity-0 scale-95"
				enterTo="transform opacity-100 scale-100"
				leave="transition ease-in duration-75"
				leaveFrom="transform opacity-100 scale-100"
				leaveTo="transform opacity-0 scale-95"
			>
				<Menu.Items className="absolute right-0 z-50 py-1 pt-2 w-48 bg-black rounded-md ring-1 ring-black ring-opacity-5 shadow-lg origin-top-right focus:outline-none">
					{/* <Menu.Item>
						  {() => (
							<Link href="profile">
							<a
							className={classNames(
							router.asPath === "/profile" ? "text-other-200" : "",
							"block px-4 py-2 text-sm text-primary-700",
							)}
							>
							Your Profile
							</a>
							</Link>
						  )}
					    </Menu.Item>
					    <Menu.Item>
						  {() => (
							<Link href="settings">
							<a
							className={classNames(
							router.asPath === "/settings" ? "text-other-200" : "",
							"block px-4 py-2 text-sm text-primary-700",
							)}
							>
							Settings
							</a>
							</Link>
						  )}
					    </Menu.Item> */}
					<Menu.Item>
						{() => (
							<button
								type="button"
								onClick={() => signOut()}
								className={classNames(
									"block px-4 py-2 text-sm text-primary-700",
								)}
							>
								Sign out
							</button>
						)}
					</Menu.Item>
				</Menu.Items>
			</Transition>
		</Menu>
	);
};
