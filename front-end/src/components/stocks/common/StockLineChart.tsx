import { curveMonotoneX } from "@visx/curve";
import { LinearGradient } from "@visx/gradient";
import { scaleLinear, scaleTime } from "@visx/scale";
import { LinePath } from "@visx/shape";
import { extent, max, min } from "d3-array";
import React, { useEffect, useState } from "react";
import useSWR from "swr";

import { fetcher } from "@/helpers/fetcher";
import { makeBackendUrl } from "@/helpers/url-helper";

import { ChangeTotal } from "./types/Calculator";
import type { GraphStock } from "./types/GraphStock";

const colors = {
	white: "#FFFFFF",
	black: "#1B1B1B",
	gray: "#98A7C0",
	darkGray: "#2A2A2A",
	accent: "#40FEAE",
	darkAccent: "#256769",
	errorAccent: "#E7376C",
	errorDarkAccent: "#370715",
};
// accessors
const getDate = (d: GraphStock) => new Date(d.timestamp);
const getStockValue = (d: GraphStock) => d.price;

type AreaProps = {
	id: number;
	width: number;
	height: number;
	margin?: { top: number; right: number; bottom: number; left: number };
};

export const StockLineChart = ({
	id,
	width,
	height,
	margin = { top: 0, right: 0, bottom: 0, left: 0 },
}: AreaProps) => {
	const [isIncreasing, setIsIncreasing] = useState(true);
	const { data, error } = useSWR(
		`${makeBackendUrl("dashboard", "stocks")}/${id}/graph`,
		fetcher,
	);

	useEffect(() => {
		if (data) {
			setIsIncreasing(
				ChangeTotal(
					getStockValue(data.prices.slice(-35).at(0)),
					getStockValue(data.prices.slice(-35).at(-1)),
				) > 0,
			);
		}
	}, [data]);

	if (width < 10) return null;
	if (error) return <div>failed to load</div>;
	if (!data) return <div>loading...</div>;

	// bounds
	const innerWidth = width - margin.left - margin.right;
	const innerHeight = height - margin.top - margin.bottom;

	// scales
	const dateScale = scaleTime({
		range: [margin.left, innerWidth + margin.left],
		domain: extent(data.prices.slice(-35), getDate) as [Date, Date],
	});

	const stockValueScale = scaleLinear({
		range: [innerHeight + margin.top, margin.top],
		domain: [
			min(data.prices.slice(-35), getStockValue) || 0,
			max(data.prices.slice(-35), getStockValue) || 0,
		],
		nice: true,
	});

	return (
		<div>
			<svg height={height} width={width}>
				<LinearGradient
					id="line-gradient"
					from={colors.accent}
					to={colors.darkAccent}
				/>

				<LinearGradient
					id="line-gradient-error"
					from={colors.errorAccent}
					to={colors.errorDarkAccent}
				/>

				{/* Actual Line */}
				<LinePath
					data={data.prices.slice(-35)}
					x={(d: GraphStock) => dateScale(getDate(d)) ?? 0}
					y={(d: GraphStock) => stockValueScale(getStockValue(d)) ?? 0}
					stroke={
						isIncreasing
							? "url('#line-gradient')"
							: "url('#line-gradient-error')"
					}
					strokeWidth={2}
					curve={curveMonotoneX}
				/>
			</svg>
		</div>
	);
};
