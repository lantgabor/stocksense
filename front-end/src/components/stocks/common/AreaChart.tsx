import { Axis } from "@visx/axis";
import { curveMonotoneX } from "@visx/curve";
import { localPoint } from "@visx/event";
import { LinearGradient } from "@visx/gradient";
import { GridColumns, GridRows } from "@visx/grid";
import { scaleLinear, scaleTime } from "@visx/scale";
import { AreaClosed, Bar, Line, LinePath } from "@visx/shape";
import {
	defaultStyles,
	Tooltip,
	TooltipWithBounds,
	withTooltip,
} from "@visx/tooltip";
import type { WithTooltipProvidedProps } from "@visx/tooltip/lib/enhancers/withTooltip";
import { extent, max, min } from "d3-array";
import React, { useCallback, useMemo } from "react";

import {
	bisectDate,
	getDate,
	getSentimentValue,
	getStockValue,
	GraphSentiment,
	GraphStock,
} from "./types/GraphStock";

type TooltipData = GraphStock;

const colors = {
	white: "#FFFFFF",
	black: "#060d0f",
	gray: "#98A7C0",
	darkGray: "#2A2A2A",
	accent: "#40FEAE",
	darkAccent: "#256769",
	errorAccent: "#E7376C",
	errorDarkAccent: "#370715",
};

const tooltipStyles = {
	...defaultStyles,
	background: "#3b6978",
	border: "1px solid white",
	color: "white",
};

type AreaProps = {
	width: number;
	height: number;
	margin?: { top: number; right: number; bottom: number; left: number };
	prices: GraphStock[];
	sentiments: GraphSentiment[];
};

export default withTooltip<AreaProps, TooltipData>(
	({
		prices,
		sentiments,
		width,
		height,
		margin = { top: 65, right: 65, bottom: 65, left: 65 },
		showTooltip,
		hideTooltip,
		tooltipData,
		tooltipTop = 0,
		tooltipLeft = 0,
	}: AreaProps & WithTooltipProvidedProps<TooltipData>) => {
		// bounds
		const innerWidth = width - margin.left - margin.right;
		const innerHeight = height - margin.top - margin.bottom;

		const dateScaleSentiments = useMemo(
			() =>
				scaleTime({
					range: [margin.left, innerWidth + margin.left],
					domain: extent(sentiments, getDate) as [Date, Date],
				}),
			[innerWidth, margin.left, sentiments],
		);

		const valueScaleSentiments = useMemo(
			() =>
				scaleLinear({
					range: [innerHeight + margin.top, margin.top],
					domain: [
						min(sentiments, getSentimentValue) || 0,
						max(sentiments, getSentimentValue) || 0,
					],
					nice: true,
				}),
			[margin.top, innerHeight, sentiments],
		);

		// scales
		const dateScaleStocks = useMemo(
			() =>
				scaleTime({
					range: [margin.left, innerWidth + margin.left],
					domain: extent(prices, getDate) as [Date, Date],
				}),
			[innerWidth, margin.left, prices],
		);

		const valueScaleStocks = useMemo(
			() =>
				scaleLinear({
					range: [innerHeight + margin.top, margin.top],
					domain: [
						min(prices, getStockValue) || 0,
						max(prices, getStockValue) || 0,
					],
					nice: true,
				}),
			[margin.top, innerHeight, prices],
		);

		// tooltip handler
		const handleTooltip = useCallback(
			(
				event:
					| React.TouchEvent<SVGRectElement>
					| React.MouseEvent<SVGRectElement>,
			) => {
				const { x } = localPoint(event) || { x: 0 };
				const x0 = dateScaleStocks.invert(x);
				const index = bisectDate(prices, x0, 1);
				const d0 = prices[index - 1];
				const d1 = prices[index];
				let d = d0;
				if (d1 && getDate(d1)) {
					d =
						x0.valueOf() - getDate(d0).valueOf() >
						getDate(d1).valueOf() - x0.valueOf()
							? d1
							: d0;
				}
				showTooltip({
					tooltipData: d,
					tooltipLeft: x,
					tooltipTop: valueScaleStocks(getStockValue(d)),
				});
			},
			[prices, showTooltip, valueScaleStocks, dateScaleStocks],
		);

		if (width < 10) return null;

		return (
			<div>
				<svg width={width} height={height}>
					{/* Colors */}
					<LinearGradient
						id="area-background-gradient"
						from={colors.darkAccent}
						to={colors.black}
					/>
					<LinearGradient
						id="area-gradient"
						from={colors.accent}
						to={colors.darkAccent}
					/>
					<LinearGradient
						id="sentiment-line-gradient"
						from={colors.errorAccent}
						to={colors.errorDarkAccent}
					/>

					{/* Y Axis Right */}
					<Axis
						scale={valueScaleSentiments}
						numTicks={5}
						left={innerWidth + margin.left}
						orientation="right"
						stroke={colors.darkGray}
						strokeWidth={1.5}
						tickStroke={colors.darkGray}
						tickLabelProps={() => ({
							fill: colors.gray,
							textAnchor: "start",
							verticalAnchor: "middle",
						})}
					/>

					{/* X Axis */}
					<Axis
						scale={dateScaleStocks}
						top={height - margin.bottom}
						orientation="bottom"
						stroke={colors.darkGray}
						strokeWidth={1.5}
						tickStroke={colors.darkGray}
						tickLabelProps={() => ({
							fill: colors.gray,
							textAnchor: "middle",
							verticalAnchor: "middle",
						})}
					/>

					{/* Y Axis */}
					<Axis
						hideZero
						scale={valueScaleStocks}
						numTicks={5}
						left={margin.left}
						orientation="left"
						stroke={colors.darkGray}
						strokeWidth={1.5}
						tickStroke={colors.darkGray}
						tickLabelProps={() => ({
							fill: colors.gray,
							textAnchor: "end",
							verticalAnchor: "middle",
						})}
						tickFormat={(value) => `${value}`}
					/>

					<AreaClosed<GraphStock>
						data={prices}
						x={(d) => dateScaleStocks(getDate(d)) ?? 0}
						y={(d) => valueScaleStocks(getStockValue(d)) ?? 0}
						yScale={valueScaleStocks}
						strokeWidth={3}
						stroke="url(#area-gradient)"
						fill="url(#area-background-gradient)"
						curve={curveMonotoneX}
					/>
					{/* Grid */}
					<GridRows
						left={margin.left}
						scale={valueScaleStocks}
						width={innerWidth}
						height={50}
						strokeDasharray="1,3"
						stroke={colors.gray}
						strokeOpacity={0.1}
						pointerEvents="none"
					/>
					<GridColumns
						top={margin.top}
						scale={dateScaleStocks}
						height={innerHeight}
						strokeDasharray="1,3"
						stroke={colors.gray}
						strokeOpacity={0.1}
						pointerEvents="none"
					/>

					{/* Bar */}
					<Bar
						x={margin.left}
						y={margin.top}
						width={innerWidth}
						height={innerHeight}
						fill="transparent"
						rx={14}
						onTouchStart={handleTooltip}
						onTouchMove={handleTooltip}
						onMouseMove={handleTooltip}
						onMouseLeave={() => hideTooltip()}
					/>
					{tooltipData && (
						<g>
							<Line
								from={{ x: tooltipLeft, y: margin.top }}
								to={{ x: tooltipLeft, y: innerHeight + margin.top }}
								stroke={colors.darkAccent}
								strokeWidth={2}
								pointerEvents="none"
								strokeDasharray="5,2"
							/>
							<circle
								cx={tooltipLeft}
								cy={tooltipTop + 1}
								r={4}
								fill="black"
								fillOpacity={0.1}
								stroke="black"
								strokeOpacity={0.1}
								strokeWidth={2}
								pointerEvents="none"
							/>
							<circle
								cx={tooltipLeft}
								cy={tooltipTop}
								r={4}
								fill={colors.darkAccent}
								stroke="white"
								strokeWidth={2}
								pointerEvents="none"
							/>
						</g>
					)}
					{/* Sentiment data */}
					<LinePath
						data={sentiments}
						x={(d) => dateScaleSentiments(getDate(d)) ?? 0}
						y={(d) => valueScaleSentiments(getSentimentValue(d)) ?? 0}
						strokeWidth={3}
						stroke="url(#sentiment-line-gradient)"
						curve={curveMonotoneX}
					/>
				</svg>
				{tooltipData && (
					<div>
						<TooltipWithBounds
							key={Math.random()}
							top={tooltipTop - 12}
							left={tooltipLeft + 12}
							style={tooltipStyles}
						>
							{`$${getStockValue(tooltipData)}`}
						</TooltipWithBounds>
						<Tooltip
							top={innerHeight + margin.top - 14}
							left={tooltipLeft}
							style={{
								...defaultStyles,
								minWidth: 72,
								textAlign: "center",
								transform: "translateX(-50%)",
							}}
						>
							{getDate(tooltipData).toLocaleDateString()}
						</Tooltip>
					</div>
				)}
			</div>
		);
	},
);
