import useSWR from "swr";

import { fetcher } from "@/helpers/fetcher";
import { makeBackendUrl } from "@/helpers/url-helper";

type Props = {
	symbol: string;
};

type PostComment = {
	id: number;
	timestamp: string;
	title: string;
	body: string;
};

type Post = {
	id: number;
	timestamp: string;
	title: string;
	body: string;
	provider: string;
	comments: PostComment[];
};

const formatDate = (timestamp: string) => {
	return new Date(timestamp).toLocaleString();
};

const PostCommentComponent = ({
	postComment,
}: {
	postComment: PostComment;
}) => {
	return (
		<div
			className="py-4 px-10 max-w-7xl"
			key={`${postComment.id}-${postComment.timestamp}`}
		>
			<div className="flex justify-between items-center text-other-900">
				<span>{formatDate(postComment.timestamp)}</span>
				<span>{postComment.title}</span>
			</div>
			<div className="mt-2">
				<p className="leading-normal text-other-600 mt-2">{postComment.body}</p>
			</div>
		</div>
	);
};

const PostCommentsComponent = (...comments: PostComment[]) => {
	if (comments.length === 0) {
		return null;
	}

	return (
		<ul>
			{comments.map((c) => (
				<PostCommentComponent postComment={c} />
			))}
		</ul>
	);
};

const PostComponent = ({ post }: { post: Post }) => {
	return (
		<li>
			<div
				key={`${post.id}-${post.timestamp}-${post.title}`}
				className="p-5 bg-secondary-800 rounded-md shadow-md"
			>
				<div className="flex justify-between items-center">
					<div className="text-xl font-black text-other-200">{post.title}</div>
					<div>{formatDate(post.timestamp)}</div>
				</div>
				<div className="pl-5">{post.body}</div>
				<div>comments: {PostCommentsComponent(...post.comments)}</div>
			</div>
		</li>
	);
};

export const StockPosts = ({ symbol }: Props) => {
	const { data: posts, error } = useSWR<Post[] | false>(
		makeBackendUrl("dashboard", "stocks", symbol, "posts"),
		fetcher,
	);

	if (error) return <div>Error</div>;
	if (posts === undefined) return <div>Loading...</div>;
	if (posts === false || posts.length === 0)
		return null; /* no posts or symbol not found  */

	return (
		<ul className="flex flex-col p-5 space-y-4">
			{posts.map((p: Post) => (
				<PostComponent post={p} />
			))}
		</ul>
	);
};
