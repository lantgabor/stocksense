import clsx from "clsx";
import * as React from "react";
import type {
	PolymorphicForwardRefExoticComponent,
	PolymorphicPropsWithoutRef,
} from "react-polymorphic-types";

import {
	Button,
	ButtonOwnProps as PrimaryButtonOwnProps,
	ButtonProps as PrimaryButtonProps,
	equilateral,
} from "./_Button";

const PrimaryButtonDefaultElement = "button";

export type { PrimaryButtonOwnProps, PrimaryButtonProps };

export const PrimaryButton: PolymorphicForwardRefExoticComponent<
	PrimaryButtonOwnProps,
	typeof PrimaryButtonDefaultElement
> = React.forwardRef(function PrimaryButton<
	T extends React.ElementType = typeof PrimaryButtonDefaultElement,
>(
	{
		size = "md",
		shape = "rectangle",
		intent = "neutral",
		className,
		...restProps
	}: PolymorphicPropsWithoutRef<PrimaryButtonOwnProps, T>,
	ref: React.ForwardedRef<Element>,
) {
	return (
		<Button<React.ElementType>
			ref={ref}
			size={size}
			shape={shape}
			intent={intent}
			className={clsx(
				"text-white dark:text-black border-transparent",
				!equilateral(shape) && {
					"px-2 py-1": size === "sm",
					"px-3 py-2": size === "md",
				},
				{
					"bg-other-900 dark:bg-other-100 hover:bg-other-600 dark:hover:bg-other-300":
						intent === "neutral",
					"bg-primary-500 dark:bg-primary-400 hover:bg-primary-400 dark:hover:bg-primary-500":
						intent === "danger",
				},
				className,
			)}
			{...restProps}
		/>
	);
});
