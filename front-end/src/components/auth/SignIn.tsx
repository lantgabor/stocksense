import { useRouter } from "next/router";
import { signIn } from "next-auth/client";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";

import { PrimaryButton } from "@/components/buttons/PrimaryButton";
import { Input } from "@/components/inputs/Input";

type Model = {
	email: string;
	password: string;
};

type ErrorType = "login-error" | "unknown-error";

type CurrentState =
	| {
			phase: "initial" | "submitting" | "success";
	  }
	| {
			phase: "error";
			error: ErrorType;
	  };

export const SignIn = () => {
	const {
		handleSubmit,
		watch,
		control,
		formState: { errors },
	} = useForm<Model>();

	const [state, setState] = useState<CurrentState>({ phase: "initial" });

	watch((_, { type }) => {
		if (type === "change" && state.phase === "error") {
			setState({ phase: "initial" });
		}
	});

	const router = useRouter();
	const canSubmit = state.phase === "initial" || state.phase === "error";

	const onSubmit = handleSubmit(async (data) => {
		setState({ phase: "submitting" });
		const result = await signIn("credentials", {
			email: data.email,
			password: data.password,
			redirect: false,
		});
		if (!result) {
			setState({ phase: "error", error: "unknown-error" });
			return;
		}

		if (!result.ok) {
			setState({ phase: "error", error: "login-error" });
			return;
		}

		setState({ phase: "success" });

		router.push({ pathname: "/stocks" });
	});

	return (
		<div className="dark text-other-200">
			{state.phase === "submitting" && <div>Login in progress...</div>}
			<form onSubmit={onSubmit}>
				<div className="flex flex-col space-y-3">
					<div>
						<Controller
							name="email"
							control={control}
							rules={{
								required: true,
								maxLength: 320,
								pattern: /^\S+@\S+$/i,
							}}
							render={({ field }) => (
								<Input
									type="text"
									placeholder="Email"
									required={errors.email?.type === "required"}
									{...field}
								/>
							)}
						/>
						{errors.email?.type === "required" && <div>Email is required</div>}
						{errors.email?.type === "maxLength" && <div>Email is too long</div>}
						{errors.email?.type === "pattern" && (
							<div>Invalid email address</div>
						)}
					</div>
					<div className="pb-11">
						<Controller
							name="password"
							control={control}
							rules={{
								required: true,
								maxLength: 255,
							}}
							render={({ field }) => (
								<Input
									type="password"
									placeholder="Password"
									required={errors.password?.type === "required"}
									{...field}
								/>
							)}
						/>
						{errors.password?.type === "required" && (
							<div>Password is required</div>
						)}
						{errors.password?.type === "maxLength" && (
							<div>Password is too long</div>
						)}
						{errors.password?.type === "minLength" && (
							<div>Password is too short</div>
						)}
						{errors.password?.type === "serverError" && (
							<div>{errors.password.message}</div>
						)}
					</div>
					{state.phase === "error" && state.error === "login-error" && (
						<div>Invalid email or password</div>
					)}
					<PrimaryButton type="submit" disabled={!canSubmit}>
						Login
					</PrimaryButton>
				</div>
			</form>
		</div>
	);
};

/* export async function getServerSideProps(context: CtxOrReq) {
 * 	return {
 * 		props: {
 * 			csrfToken: await getCsrfToken(context),
 * 		},
 * 	};
 * } */
