import { signIn, useSession } from "next-auth/client";
import * as React from "react";

type WithAuthProps = {
	children: React.ReactNode;
};

export default function WithAuth({ children }: WithAuthProps) {
	const [session, loading] = useSession();
	const hasUser = !!session?.user;
	React.useEffect(() => {
		if (loading) {
			// do nothing while loading
			return;
		}

		if (!hasUser) {
			signIn();
		}
	}, [hasUser, loading]);

	if (hasUser) {
		return <>{children}</>;
	}

	return <div>Loading...</div>;
}
