import { useRouter } from "next/router";
import React, { useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";

import { PrimaryButton } from "@/components/buttons/PrimaryButton";
import { Input } from "@/components/inputs/Input";
import { createAnonymPostFetcher } from "@/helpers/backend-fetcher";
import { makeBackendUrl } from "@/helpers/url-helper";

type Model = {
	email: string;
	password: string;
	password2: string;
	firstName: string;
	lastName: string;
	plan: "free" | "paid";
};

type ServerValidationError = {
	email?: string[];
	password?: string[];
};

type Phase = "initial" | "submitting" | "error" | "success";

export default function RegisterComponent() {
	const {
		register,
		handleSubmit,
		setError,
		watch,
		control,
		formState: { errors },
	} = useForm<Model>();

	const [phase, setPhase] = useState<Phase>("initial");
	const router = useRouter();
	const canSubmit = phase === "initial" || phase === "error";
	const password = useRef({});
	password.current = watch("password", "");

	const onSubmit = handleSubmit(async (data) => {
		const url = makeBackendUrl("trader", "register");
		const fetcher = createAnonymPostFetcher<ServerValidationError>(url);

		const input = {
			email: data.email,
			first_name: data.firstName,
			last_name: data.lastName,
			password: data.password,
			password2: data.password2,
			trader: {
				plan: data.plan,
			},
		};

		setPhase("submitting");
		const result = await fetcher(input);

		if (!result.success) {
			setPhase("error");
			if (result.error.code === "validation-error") {
				const { error } = result;
				const { details } = error;

				if (details.email) {
					setError("email", {
						type: "alreadyUsed",
						message: details.email[0],
					});
				}
				if (details.password) {
					setError("password", {
						type: "serverError",
						message: details.password[0],
					});
				}
			} else {
				alert("Unknown error occurred, please try again later");
			}

			return;
		}

		setPhase("success");
	});

	if (phase === "success") {
		router.push({ pathname: "/login" });
		return (
			<>
				<div>Registered :)</div>;
			</>
		);
	}

	return (
		<div className="flex py-8 px-2 sm:px-6 lg:px-8 mx-auto max-w-7xl text-other-700">
			<section className="py-16 px-4 sm:px-6 lg:px-4 mx-auto max-w-6xl">
				<div className="dark">
					{phase === "submitting" && <div>Registering...</div>}
					{phase === "error" && <div>Error</div>}
					<form onSubmit={onSubmit}>
						<div className="flex flex-col space-y-3 ">
							<div className="text-other-200">
								<Controller
									name="firstName"
									control={control}
									rules={{
										required: true,
										maxLength: 255,
									}}
									render={({ field }) => (
										<Input
											type="text"
											placeholder="First name"
											required={errors.firstName?.type === "required"}
											{...field}
										/>
									)}
								/>

								{errors.firstName?.type === "required" && (
									<div>First name is required</div>
								)}
								{errors.firstName?.type === "maxLength" && (
									<div>First name is too long</div>
								)}
							</div>
							<div className="text-other-200">
								<Controller
									name="lastName"
									control={control}
									rules={{
										required: true,
										maxLength: 255,
									}}
									render={({ field }) => (
										<Input
											type="text"
											placeholder="Last name"
											required={errors.lastName?.type === "required"}
											{...field}
										/>
									)}
								/>
								{errors.lastName?.type === "required" && (
									<div>Last name is required</div>
								)}
								{errors.lastName?.type === "maxLength" && (
									<div>Last name is too long</div>
								)}
							</div>
							<div className="text-other-200">
								<Controller
									name="email"
									control={control}
									rules={{
										required: true,
										maxLength: 320,
										pattern: /^\S+@\S+$/i,
									}}
									render={({ field }) => (
										<Input
											type="email"
											placeholder="Email"
											required={errors.email?.type === "required"}
											{...field}
										/>
									)}
								/>
								{errors.email?.type === "required" && (
									<div>Email is required</div>
								)}
								{errors.email?.type === "maxLength" && (
									<div>Email is too long</div>
								)}
								{errors.email?.type === "pattern" && (
									<div>Invalid email address</div>
								)}
								{errors.email?.type === "alreadyUsed" && (
									<div>The email address is already used</div>
								)}
							</div>
							<div className="text-other-200">
								<Controller
									name="password"
									control={control}
									rules={{
										required: true,
										minLength: 8,
										maxLength: 255,
									}}
									render={({ field }) => (
										<Input
											type="password"
											placeholder="Password"
											required={errors.password?.type === "required"}
											{...field}
										/>
									)}
								/>
								{errors.password?.type === "required" && (
									<div>Password is required</div>
								)}
								{errors.password?.type === "maxLength" && (
									<div>Password is too long</div>
								)}
								{errors.password?.type === "minLength" && (
									<div>Password is too short</div>
								)}
								{errors.password?.type === "serverError" && (
									<div>{errors.password.message}</div>
								)}
							</div>
							<div className="text-other-200">
								<Controller
									name="password2"
									control={control}
									rules={{
										required: true,
										minLength: 8,
										maxLength: 255,
										validate: (value) =>
											value === password.current ||
											"The passwords do not match",
									}}
									render={({ field }) => (
										<Input
											type="password"
											placeholder="Password again"
											required={errors.password2?.type === "required"}
											{...field}
										/>
									)}
								/>
								{errors.password?.type === "required" && (
									<div>Password Again is required</div>
								)}
								{errors.password?.type === "maxLength" && (
									<div>Password Again is too long</div>
								)}
								{errors.password?.type === "minLength" && (
									<div>Password Again is too short</div>
								)}
								{errors.password2?.type === "validate" && (
									<div>{errors.password2.message}</div>
								)}
							</div>
							<div className="flex bg-transparent rounded-md border-2 border-opacity-30">
								<span className="inline-flex items-center px-3 text-other-200 rounded-l-md">
									Plan:
								</span>
								<select
									className="block flex-1 col-start-1 row-start-1 px-3.5 w-full h-11 bg-transparent rounded-none rounded-r-md focus:ring ring-offset-1 dark:ring-offset-black focus:outline-none placeholder-gray-600/80 dark:placeholder-gray-400/80 ring-blue-500/50 dark:ring-blue-400/50 invalid:ring-error-500/50 dark:invalid:ring-error-400/50"
									{...register("plan", { required: true })}
								>
									<option value="free">Free</option>
									<option value="standard">Standard</option>
								</select>
								{errors.plan?.type === "required" && (
									<div>Plan is required</div>
								)}
							</div>
							<PrimaryButton type="submit" disabled={!canSubmit}>
								Register
							</PrimaryButton>
						</div>
					</form>
				</div>
			</section>
		</div>
	);
}
