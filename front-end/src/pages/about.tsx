import React from "react";

import { Footer } from "@/components/footer/Footer";
import { Hero } from "@/components/hero/Hero";

export default function aboutPage() {
	return (
		<>
			<Hero />
			<footer>
				<Footer />
			</footer>
		</>
	);
}
