import React from "react";

import { CardContainer } from "@/components/content/CardContainer";
import { Mobile } from "@/components/content/Mobile";
import { Footer } from "@/components/footer/Footer";
import { Hero } from "@/components/hero/Hero";

export default function Page() {
	return (
		<>
			<Hero />
			<CardContainer />
			<Mobile />
			<footer>
				<Footer />
			</footer>
		</>
	);
}
