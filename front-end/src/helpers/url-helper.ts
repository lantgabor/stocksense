const BASE_URL = process.env.PCFG_BACKEND_API_BASE || "";
const BASE_URL_PUBLIC = process.env.NEXT_PUBLIC_BACKEND_API_BASE || "";

function makeUrl(type: "public" | "internal", ...components: string[]) {
	const base = type === "public" ? BASE_URL_PUBLIC : BASE_URL;
	components = [base, ...components];

	const url = components.reduce((prev, cur) => {
		if (prev.length === 0) {
			return cur;
		}

		let res = prev;
		if (!res.endsWith("/")) {
			res += "/";
		}

		return res + cur;
	}, "");

	return url;
}

export function makeBackendUrl(...components: string[]) {
	return makeUrl("public", ...components);
}

export function makeInternalBackendUrl(...components: string[]) {
	return makeUrl("internal", ...components);
}

export function backendAuthorizationHeader(accessToken: string) {
	return {
		Authorization: `Bearer ${accessToken}`,
	};
}
