import { useEffect, useState } from "react";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";

const useDebounce = <T>(
	time: number,
	initialValue: T,
): [T, (value: T) => void] => {
	const [value, setValue] = useState(initialValue);
	const [values] = useState(() => new Subject<T>());

	useEffect(() => {
		const subscription = values.pipe(debounceTime(time)).subscribe(setValue);
		return () => subscription.unsubscribe();
	}, [time, values]);

	return [value, (v: T) => values.next(v)];
};

export default useDebounce;
